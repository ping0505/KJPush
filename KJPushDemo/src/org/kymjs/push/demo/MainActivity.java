package org.kymjs.push.demo;

import org.kymjs.push.core.KJPushManager;

import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aty_main);
        KJPushManager.create().startWork(this, MyReceiver.class);
    }
}
